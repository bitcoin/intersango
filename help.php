<div class='content_box'>
<h3>Help</h3>
<p>For safety, please keep funds off-site when not trading.</p>
<p>To begin trading, navigate to the <a href="?page=">front page</a>, enter your desired amount and click buy. The trade will materialise in the orderbook and under your profile. You will not be able to view any info from other traders accounts for privacy reasons.</p>
<p>You may cancel an order by viewing the desired order (found under your profile). Click cancel to revoke any current orders. You will not get a full refund on partially completed orders.</p>
<p>The source code for this website is available online at <a href="http://gitorious.org/intersango/">Gitorious</a>. We believe hiding the workings of a website is a poor substitute for good security.</p>
</div>

<div class='content_box'>
<h3>Contact info</h3>
<?php if ($loggedin) { ?>
<p>genjix@riseup.net</p>
<p>Britcoin is currently operating unlicensed. We are seeking legitimisation. Until then, all transactions are free.</p>
<?php } else { ?>
<p>Sign in to view these details.</p>
<?php } ?>
</div>

<div class='content_box'>
<h3>Terms and conditions</h3>
<p><b>NOTICE.</b> This web site is offered to you conditioned on your acceptance without modification of the terms, conditions and notices contained herein. Your use of the site constitutes your agreement to comply with these terms and conditions.</p>
<p>1. This site is an early public-alpha. Be prepared for problems. We are currently at this stage not liable for mistakes made but this will change in the future. We will do our best to resolve issues. In no part are we liable.</p>
<p>2. In the case of a dispute between parties, we will not intervene to settle the matter. Once an order has been filled, and funds appear in the parties accounts, that trade should be consider irrevocable. Trades are irreversible.</p>
<p>3. As login security is handled off-site by your OpenID provider, you are responsible for securing and recovering stolen or cracked logins.</p>
<p>4. Any attempt at this site's security will be met with immediate seizure of involved accounts and funds. Victims of fraud will not receive refunds, but are notified and adviced on how to prevent future problems.</p>
<p>5. We do not claim ownership of any materials you provide to us (including feedback and suggestions). However, you are granting us and our agents permission to use your submissions in connection with the operation of this web site and it's other businesses, including without limitation, the right to copy, distribute, transmit, publicly display, reproduce and edit your submission all without payment of compensation to you for any such usage.</p>
<p>6. The information contained on here has no regard to the specific investment objective, financial situation or particular needs of any specific recipient. We do not endorse any particular financial products. Posted content is solely for informational purposes. Please consult appropriate professionals for specific advice tailored to your situation.</p>
</div>

<div class='content_box'>
<h3>Privacy policy</h3>

<h4>A. Policy statement</h4>
<p>Britcoin respects customer privacy. Users can visit most pages on our website and use all of the tools located without identifying themselves or providing personal or financial information. We collect the minimum information needed, which is all viewable from your profile. Normally we do not expunge records but will on request after careful consideration. We do not store cookies for privacy purposes. Our website may provide links to third-party websites for authentication, customer convenience and information. We do not control those third-party sites or their privacy practices.</p>

<h4>B. How we use personal information</h4>
<p>We may use personal information as necessary to: complete bank deposits and withdrawals, effect transactions authorised by the user and maintain accounts.</p>
<p>We will not use or disclose any personal information from our users to any third parties except as stated in this policy. Personal information may be transferred across national and state borders for the purposes of data consolidation, storage and customer data management.</p>

<h4>C. Disclosure of personal information</h4>
<p>We may disclose personal information of current and former users to affiliated and non-affiliated third party entities in the following events:</p>
<p>
    <ul class='list'>
        <li>To business partners performing services on our behalf under written agreements restricting their usage of personal information. Restrictions are provided for limited purposes and to refrain from further use or disclosure.</li>
        <li>In the ordinary course of business to our attorneys, accountants and auditors.
        <li>To persons holding a legal or beneficial interest relating to the customer's account.</li>
        <li>To persons acting in a fiduciary, representative, or attorney capacity in relation to an account.</li>
        <li>To protect against actual or potential fraud, unauthorized transactions, claims or other liability.</li>
        <li>To government, regulatory or law enforcement agencies to the extent permitted or required by law, or to comply with applicable legal requirements.</li>
        <li>To comply with civil, criminal or regulatory investigations, or judicial process, subpoena, summons or warrant by federal, state or local authorities.</li>
    </ul>
</p>

<h4>D. Permission to disclose information</h4>
<p>Except as permitted in this policy, we will not share personal information about any present or former customer with any non-affiliated third party without the customer's prior written consent or providing the customer with a clear notice, an opportunity to "opt out" of that disclosure, and information on how to opt out.</p>

<h4>E. Notification of policy changes</h4>
<p>Updates will be posted on our front page with an offer to re-review our changes. Our customers will always know what information we collect online, how we use it and what choices they have.</p>

<p>We value our customer's opinions. Do not hesitate to email us if you have comments or questions about our policies. Details are found above.</p>
</div>

